/**
* @name cuenta
* @version 1.0.0
* @type Modulo
* @description Este archivo contiene un conjunto de métodos que implementan
*              el manejo de cuentas en la aplicación.
* @author PCURBELO
*/

var requestJSON = require('request-json');
let constantes = require('./constantes');
const apikeyMLab = constantes.apikeyMLab;
const baseMLabURL = constantes.baseMLabURL;

/**
* @name crearCuenta
* @param params.idUsuario (numérico), body.IBAN (string), body.moneda(string UYU o USD)
* @version 1.0.0
* @type method
* @description Crea una cuenta asociada al usuario indicado
* @author PCURBELO
*/
function crearCuenta(req, res) {
  var respuesta = {};
  var idUsuario = req.params.id;
  var newCta = {
    "id_cuenta": 0,
    "id_usuario": parseInt(req.params.id),
    "IBAN": req.body.IBAN,
    "moneda": req.body.moneda,
    "saldo": 0.00,
    "movimientos": []
  };

  if (req.body.IBAN == undefined || req.body.moneda == undefined) {
    respuesta = {"msg": "Es necesario pasar todos los parámetros."};
    res.status(400).send(respuesta);
  } else {
    var queryString = 'q={"id": ' + idUsuario + '}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' + queryString + apikeyMLab,
      function(error, respuestaMLab, body) {
        if (error) {
          respuesta = {"msg": "Error obteniendo usuario."};
          res.status(500).send(respuesta);
        } else {
          if (body.length > 0) {
            var queryString1 = 'q={"nombre":"cuenta"}&';
            var consulta = 'guias?' + queryString1 + apikeyMLab;
            httpClient.get(consulta,
              function(error1, respuestaMLab1, body1) {
                if (error1) {
                  respuesta = {"msg": "Error al leer numerador de cuentas."};
                  res.status(500).send(respuesta);
                } else {
                  body1[0].valor += 1;
                  httpClient.put('guias/' + body1[0]._id.$oid + '?' + apikeyMLab, body1[0],
                    function(error2, respuestaMLab2, body2) {
                      if (error2) {
                        respuesta = {"msg": "Error al grabar numerador de cuentas."};
                        res.status(500).send(respuesta);
                      } else {
                        var id_cuenta = body1[0].valor;
                        newCta.id_cuenta = id_cuenta;
                        console.log(id_cuenta);
                        httpClient.post('cuenta?' + apikeyMLab, newCta,
                          function(error3, respuestaMLab3, body3) {
                            if (error3) {
                              respuesta = {"msg": "Error al grabar la nueva cuenta."};
                              res.status(500).send(respuesta);
                            } else {
                              respuesta = {"msg": "La cuenta ha sido creada"};
                              res.status(201).send(respuesta);
                            }
                          });
                      }
                    });
                  }
                });
              } else {
                respuesta = {"msg": "Usuario no encontrado."};
                res.status(404).send(respuesta);
              }
            }
          });
        };
}; //FIN crearCuenta

/**
* @name borrarCuenta
* @param params.IdCuenta (numérico), body.idUsuario (numérico)
* @version 1.0.0
* @type method
* @description Borra la cuenta indicada validando que esté asociada al usuario indicado
* @author PCURBELO
*/
function borrarCuenta(req, res) {
  var respuesta = {};
  var IdCuenta = req.params.id_cuenta;
  var idUsuario = req.params.id;
  var queryString = 'q={"id_cuenta": ' + IdCuenta + ', "id_usuario":' + idUsuario + '}&';
  var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('cuenta?' + queryString + apikeyMLab,
      function(error, respuestaMLab, body) {
        if (error) {
          respuesta = {"msg": "Error obteniendo cuenta."};
          res.status(500).send(respuesta);
        } else {
          console.log("--> Respuesta:" + JSON.stringify(body));
          if (body.length > 0) {
            if (body[0].movimientos == undefined && body[0].saldo == 0) {
              httpClient.delete('cuenta/' + body[0]._id.$oid + '?' + apikeyMLab,
                function(error2, respuestaMLab2, body2) {
                if (error2) {
                  respuesta = {"msg": "Error al borrar cuenta. "};
                  res.status(500).send(respuesta);
                } else {
                  respuesta = {"msg": "Cuenta borrada correctamente. ", body2};
                  res.status(200).send(respuesta);
                }
              });
            } else {
              if (body[0].movimientos.length != 0) {
                respuesta = {"msg": "La cuenta tiene movimientos, no puede borrarse."};
                res.status(401).send(respuesta);
              } else {
                respuesta = {"msg": "El saldo de La cuenta no es cero, no puede borrarse."};
                res.status(401).send(respuesta);
              };
            };
          } else {
            respuesta = {"msg": "Cuenta no existe. "};
            res.status(500).send(respuesta);
          };
        };
      });
} //FIN borrarCuenta

/**
* @name obtenerCuentas
* @param params.idUsuario (numérico)
* @version 1.0.0
* @type method
* @description Obtener las cuentas asociadas al usuario indicado
* @author PCURBELO
*/
function obtenerCuentas(req, res) {
  var respuesta = {};
  var idUsuario = req.params.id;
  var queryStrSort = 's={"cuenta.id":1}&';
  var queryString = 'q={"id_usuario": ' + idUsuario + '}&';
  var queryStrField = 'f={"_id":0, "id_usuario":0, "movimientos":0}&';
  console.log("--> Usuario recibido:" + idUsuario);
  var httpClient = requestJSON.createClient(baseMLabURL);
  httpClient.get('cuenta?' + queryString + queryStrField + queryStrSort + apikeyMLab,
    function(error, respuestaMLab, body) {
      if (error) {
        respuesta = {"msg": "Error obteniendo lista de cuentas."};
        res.status(500).send(respuesta);
      } else {
        if (body.length != undefined || body.length > 0) {
          console.log("--> Respuesta OK: obtenerCuentas");
          console.log("--> Respuesta: obtenerCuentas" + JSON.stringify(body));
          respuesta = body;
          res.status(200).send(respuesta);
        } else {
          respuesta = {"msg": "El usuario no tiene cuentas asociadas."};
          res.status(404).send(respuesta);
        }
      }
    });
} //FIN obtenerCuentas


/**
* @name crearMov
* @param params.idUsuario (numérico), params.idCuenta (numérico), body.tipo(string), body.descripcion(string)
* @version 1.0.0
* @type method
* @description Crea un movimiento asociado al usuario y cuenta indicado
* @author PCURBELO
*/
function crearMov(req, res) {
  var respuesta = {};
  var idUsuario = req.params.id;
  var IdCuenta = req.params.id_cuenta;
  var hoy = new Date();
  var fechaHoy = hoy.getDate() + "/" + (hoy.getMonth() + 1) + "/" + hoy.getFullYear();
  var newMov = {
    "id": 0,
    "fecha": fechaHoy,
    "tipo": req.body.tipo,
    "importe": parseInt(req.body.importe),
    "descripcion": req.body.descripcion
  };

  if ((req.body.tipo == "db" || req.body.tipo == "cr") && req.body.importe > 0) {
    var queryString = 'q={"id_usuario": ' + idUsuario + ', "id_cuenta":' + IdCuenta + '}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
      httpClient.get('cuenta?' + queryString + apikeyMLab,
        function(error, respuestaMLab, body) {
          if (error) {
            respuesta = {"msg": "Error obteniendo usuario/cuenta."};
            res.status(500).send(respuesta);
          } else {
            if (body.length > 0) {
              var queryString1 = 'q={"nombre":"movimiento"}&';
              var consulta = 'guias?' + queryString1 + apikeyMLab;
                httpClient.get(consulta,
                  function(error1, respuestaMLab1, body1) {
                    if (error1) {
                      respuesta = {"msg": "Error al leer numerador de movimientos."};
                      res.status(500).send(respuesta);
                    } else {
                      body1[0].valor += 1;
                      httpClient.put('guias/' + body1[0]._id.$oid + '?' + apikeyMLab, body1[0],
                        function(error2, respuestaMLab2, body2) {
                          if (error2) {
                            respuesta = {"msg": "Error al grabar numerador de movimientos."};
                            res.status(500).send(respuesta);
                          } else {
                              var idMovimiento = body1[0].valor;
                              newMov.id = idMovimiento;
                              body[0].movimientos.push(newMov);

                              if (req.body.tipo == 'db') {
                                body[0].saldo -= parseInt(req.body.importe);
                              } else {
                                body[0].saldo += parseInt(req.body.importe);
                              }
                              var cuentaModif = {
                                "id_usuario": body[0].id_usuario,
                                "IBAN": body[0].IBAN,
                                "moneda": body[0].moneda,
                                "saldo" : body[0].saldo,
                                "movimientos" : body[0].movimientos
                              }

                              var ctaModifStr = '{"$set":' + JSON.stringify(cuentaModif) + '}';
                              httpClient.put('cuenta?' + queryString + apikeyMLab, JSON.parse(ctaModifStr),
                                function(error4, respuestaMLab4, body4) {
                                  if (error4) {
                                    respuesta = {"msg": "Error al grabar movimiento."};
                                    res.status(500).send(respuesta);
                                  } else {
                                    respuesta = {"msg": "El movimiento ha sido creado."};
                                    res.status(201).send(respuesta);
                                      }
                                    }); //4
                                  }
                                }); //2
                              }
                            });//1
                          }
                        }
                      });//0
                } else {
                  if (req.body.importe <= 0) {
                    respuesta = {"msg": "Importe debe de ser mayor que cero."};
                    res.status(401).send(respuesta);
                  } else {
                    respuesta = {"msg": "TIpo de movimiento debe ser db o cr."};
                    res.status(401).send(respuesta);
                  }
                }
  }//****** crearMov                ******

  /**
  * @name obtenerMovs
  * @param params.idUsuario (numérico)
  * @version 1.0.0
  * @type method
  * @description Obtener los movimientos asociados al usuario y cuenta indicado
  * @author PCURBELO
  */
  function obtenerMovs(req, res) {
    var respuesta = {};
    var idUsuario = req.params.id;
    var IdCuenta = req.params.id_cuenta;
    var queryString = 'q={"id_usuario": ' + idUsuario + ', "id_cuenta":' + IdCuenta + '}&';
    var queryStrField = 'f={"_id":0, "id_usuario":0, "id_cuenta":0, "IBAN":0, "moneda":0, "saldo":0,}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('cuenta?' + queryString + queryStrField + apikeyMLab,
      function(error, respuestaMLab, body) {
        if (error) {
          respuesta = {"msg": "Error obteniendo lista de movimientos."};
          res.status(500).send(respuesta);
        } else {
          if (body.length != undefined || body.length > 0) {
            console.log("--> Respuesta OK: obtenerCuentas");
            console.log("--> Respuesta: obtenerCuentas" + JSON.stringify(body));
            respuesta = body;
            res.status(200).send(respuesta);
          } else {
            respuesta = {"msg": "El usuario no tiene movimientos asociados."};
            res.status(404).send(respuesta);
          }
        }
      });
  } //FIN obtenerMovs

//Metodos visibles
module.exports = {
  crearCuenta,
  borrarCuenta,
  obtenerCuentas,
  crearMov,
  obtenerMovs
}
