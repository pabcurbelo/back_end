/**
 * @name app
 * @version 1.0.0
 * @type Main
 * @description Este archivo es el inicio de la aplicación. Maneja las peticiones.
 * @author PCURBELO
 */

var express = require("express");
var bodyParser = require('body-parser');
var app = express();
var requestJSON = require('request-json');

let constantes = require('./constantes');
var usuarioFunc = require('./usuario');
var cuentaFunc = require('./cuenta');

const port = constantes.port;
const URLbase = constantes.URLbase;
const apikeyMLab = constantes.apikeyMLab;
const baseMLabURL = constantes.baseMLabURL;

console.log("Puerto:" + port);
console.log("URLbase:" + URLbase);
console.log("apikeyMLab:" + apikeyMLab);
console.log("baseMLabURL:" + baseMLabURL);

app.use(bodyParser.json());
app.listen(constantes.port);
console.log('Aplicacion iniciada - Escuchando en el puerto' + port);

// GET users - obtener lista de usuarios
app.get(URLbase + 'users', usuarioFunc.obtenerUsuarios);

// GET users con id - obtener usuario a partir del id
app.get(URLbase + 'users/:id', usuarioFunc.obtenerUsuario);

// POST users - crear usuario
app.post(URLbase + 'users', usuarioFunc.crearUsuario);

//PUT users con id - modificar usuario a partir de id
app.put(URLbase + 'users/:id', usuarioFunc.modificarUsuario);

// DELETE users con id - borra usuario a partir de id
app.delete(URLbase + 'users/:id', usuarioFunc.borrarUsuario);

//POST login - loguea un usuario validando su email y contraseña
app.post(URLbase + "login", usuarioFunc.login);

//POST logout - desloguea un usuario validando su email y estado logueado
app.post(URLbase + "logout", usuarioFunc.logout);

// POST cuenta - crea una cuenta asociada al usuario recibido
app.post(URLbase + 'users/:id/cuentas', cuentaFunc.crearCuenta);

// POST cuenta - borra una cuenta asociada al usuario recibido
app.delete(URLbase + 'users/:id/cuentas/:id_cuenta', cuentaFunc.borrarCuenta);

// GET cuentas - retorna una lista de cuentas asociadas al usuario
app.get(URLbase + 'users/:id/cuentas', cuentaFunc.obtenerCuentas);

// POST movimientos - crea un movimiento asociado al usuario y cuenta recibidos
app.post(URLbase + 'users/:id/cuentas/:id_cuenta/movimientos', cuentaFunc.crearMov);

// GET movimientos - retorna una lista de movimientos asociado al usuario y cuenta recibidos
app.get(URLbase + 'users/:id/cuentas/:id_cuenta/movimientos', cuentaFunc.obtenerMovs);
