var express = require("express");
var bodyParser = require('body-parser');
var app = express();
var usersFile = require('./Users.json');
var port = process.env.PORT || 3000;

const URI = '/api-uruguay/v1/';

app.use(bodyParser.json());
app.listen(port);
console.log('Escuchando en el puerto' + port);

//GET users
app.get(URI + 'users' ,
  function(req, res) {
      res.send(usersFile);
});

//GET users con ID
app.get(URI + 'users/:id' ,
  function(req, res) {
    let idUser = req.params.id;
    res.send(usersFile [idUser - 1]);
//  res.sendFile('Users.json', {root: __dirname});
});

//POST users
app.post(URI + 'users' ,
    function(req, res) {
      console.log('POST users');
      let tam = usersFile.length+1;

      var userNuevo = {
        'id': tam,
        'first_name': req.body.first_name,
        'last_name':  req.body.last_name,
        'email': req.body.email,
        'password': req.body.password
      };

      usersFile.push(userNuevo);
      res.send(userNuevo);
});

//POST users
app.post(URI + 'users_post' ,
    function(req, res) {
      console.log('POST users');
      let tam = usersFile.length+1;
      var userNuevo = {
        'id': tam,
        'first_name': req.headers.first_name,
        'last_name':  req.headers.last_name,
        'email': req.headers.email,
        'password': req.headers.password
      };
      usersFile.push(userNuevo);
      res.send(userNuevo);
});

//PUT users
app.put(URI + 'users/:id' ,
    function(req, res) {
      console.log('PUT users');
      let idUpdate = req.params.id;
      var existe = true;

      if (usersFile[idUpdate-1] == undefined) {
        console.log('Usuario a modificar NO existe');
        existe = false;
      } else {
        var userNuevo = {
          'id': idUpdate,
          'first_name': req.body.first_name,
          'last_name':  req.body.last_name,
          'email': req.body.email,
          'password': req.body.password
        };
        usersFile[idUpdate-1] = userNuevo;
      }

      var msgResponse = existe ? {'msg':'Update OK'} : {'msg':'Update FALSE'};
      var statusCode = existe ? 200 : 400
      res.status(statusCode).send(msgResponse)
});

//DELETE users
app.delete(URI + 'users/:id' ,
    function(req, res) {
      console.log('DELETE users');
      let idUpdate = req.params.id;
      var existe = true;

      if (usersFile[idUpdate-1] == undefined) {
        console.log('Usuario a eliminar NO existe');
        existe = false;
      } else {
        usersFile.splice(idUpdate-1, 1);
      }

      var msgResponse = existe ? {'msg':'Delete OK'} : {'msg':'Delete FALSE'};
      var statusCode = existe ? 200 : 400
      res.status(statusCode).send(msgResponse)
});


// GET con query string
app.get(URI + "usersCons" ,
  function (req, res) {
    console.log('GET con query string');

    var encontrado = false;
    let msgResponse = {'msg':'Usuario NO encontrado'};

    for (var user in usersFile) {

      if (usersFile[user].first_name == req.query.qname && usersFile[user].last_name == req.query.qlast){
        var userRes = usersFile[user];
        msgResponse = {'msg':'Usuario encontrado', userRes };
        encontrado = true;
      }
    }
    res.send(msgResponse);
  });
