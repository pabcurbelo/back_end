const port = process.env.PORT || 3000;
const URLbase = '/api-uruguay/v1/';
const apikeyMLab  = "apiKey=cV9FHeh1bhz-R3vh8uChlEapMFGYYweZ";
const baseMLabURL = "https://api.mlab.com/api/1/databases/techubduruguay1dd/collections/";

exports.port = port;
exports.URLbase = URLbase;
exports.apikeyMLab = apikeyMLab;
exports.baseMLabURL = baseMLabURL;
