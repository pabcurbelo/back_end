/**
* @name usuario
* @version 1.0.0
* @type Modulo
* @description Este archivo contiene un conjunto de métodos que implementan
*              el manejo de usuarios en la aplicación.
* @author PCURBELO
*/
var requestJSON = require('request-json');
let constantes = require('./constantes');
const apikeyMLab = constantes.apikeyMLab;
const baseMLabURL = constantes.baseMLabURL;

/**
* @name obtenerUsuarios
* @param
* @version 1.0.0
* @type method
* @description Obtiene todos los usuarios consumiendo API REST de mLab
* @author PCURBELO
*/
function obtenerUsuarios(req, res) {
    var httpClient = requestJSON.createClient(baseMLabURL);
    var queryString = 'f={"_id":0}&';
    httpClient.get('user?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body) {
        var respuesta = {};
        if(err) {
            respuesta = {"msg" : "Error obteniendo usuarios."};
            res.status(500).send(respuesta);
        } else {
          if(body.length > 0) {
            respuesta = body;
            res.status(200).send(respuesta);
          } else {
            respuesta = {"msg" : "Sin usuarios a desplegar."};
            res.status(404).send(respuesta);
          }
        }
        //res.send(respuesta);
      });
}  //FIN GET obtenerUsuarios

/**
* @name obtenerUsuario
* @param params.id (numérico)
* @version 1.0.0
* @type method
* @description Obtiene usuario indicado en el id
* @author PCURBELO
*/
function obtenerUsuario(req, res) {
    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body){
        var respuesta = {};
        if(err) {
            respuesta = {"msg" : "Error obteniendo usuario."};
            res.status(500).send(respuesta);
        } else {
          if(body.length > 0) {
            respuesta = body;
            res.status(200).send(respuesta);
          } else {
            respuesta = {"msg" : "Usuario no encontrado."};
            res.status(404).send(respuesta);
          }
        }
      });
} // FIN GET obtenerUsuario con id

/**
* @name crearUsuario
* @param body.first_name (string), body.last_name (string), body.email (string), body.password (string)
* @version 1.0.0
* @type method
* @description Crea usuario que no exista según email
* @author PCURBELO
*/
function crearUsuario(req, res){
    var respuesta = {};
    newID = 0;
    var newUser = {
      "id" : newID,
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : req.body.password
    };

    var queryStringEmail = 'q={"email": "' + newUser.email + '"}&';
    var queryStrField = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' + queryStringEmail + queryStrField + apikeyMLab,
      function(error, respuestaMLab, body) {
        if (error) {
          respuesta = {"msg": "Error obteniendo usuario."};
          res.status(500).send(respuesta);
        } else {
          if (body.length == undefined || body.length < 1) {
            var queryString1 = 'q={"nombre":"usuario"}&';
            var consulta = 'guias?' + queryString1 + apikeyMLab;
            httpClient.get(consulta,
              function(error1, respuestaMLab1, body1) {
                if (error1) {
                  respuesta = {"msg": "Error al leer guia para numerador de usuarios."};
                  res.status(500).send(respuesta);
                } else {
                  body1[0].valor += 1;
                  httpClient.put('guias/' + body1[0]._id.$oid + '?' + apikeyMLab, body1[0],
                    function(error2, respuestaMLab2, body2) {
                      if (error2) {
                        respuesta = {"msg": "Error al grabar numerador de usuarios."};
                        res.status(500).send(respuesta);
                      } else {
                        newID = body1[0].valor;
                        newUser.id = newID;
                        httpClient.post('user?' + apikeyMLab, newUser,
                          function(error3, respuestaMLab3, body3) {
                            if (error3) {
                              respuesta = {"msg": "Error al grabar nuevo usuario."};
                              res.status(500).send(respuesta);
                            } else {
                              respuesta = {"msg": "El usuario ha sido dado de alta."};
                              res.status(201).send(respuesta);
                            }
                          });
                        }
                      });
                  }
                });
              } else {
                respuesta = {"msg": "El usuario ya existe."};
                res.status(403).send(respuesta);
              }
            }
        });
  }// FIN POST crearUsuario

/**
* @name modificarUsuario
* @param params.id (numérico), body.first_name (string), body.last_name (string), body.email (string), body.password (string)
* @version 1.0.0
* @type method
* @description Modifica un usuario existente
* @author PCURBELO
*/
function modificarUsuario(req, res){
    var respuesta = {};
    var id = req.params.id;
    var queryStringID = 'q={"id":' + id + '}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?'+ queryStringID + apikeyMLab ,
      function(error, respuestaMLab , body) {
        if (error) {
          respuesta = {"msg": "Error obteniendo usuario."};
          res.status(500).send(respuesta);
        } else {
          if (body.length > 0) {
            var usuarioModif = {
              "first_name": body[0].first_name,
              "last_name": body[0].last_name,
              "email": body[0].email,
              "password" : body[0].password
            }
            if (req.body.first_name != undefined) {
              usuarioModif.first_name = req.body.first_name
            }
            if (req.body.last_name != undefined) {
              usuarioModif.last_name = req.body.last_name
            }
            if (req.body.email != undefined) {
              usuarioModif.email = req.body.email
            }
            if (req.body.password != undefined) {
              usuarioModif.password = req.body.password
            }

            var usrModifStr = '{"$set":' + JSON.stringify(usuarioModif) + '}';
            httpClient.put('user?q={"id": ' + id + '}&' + apikeyMLab, JSON.parse(usrModifStr),
              function(error1, respuestaMLab1, body1) {
                if (!error1) {
                  respuesta = {"msg": "Usuario actualizado."};
                  res.status(200).send(respuesta);
                } else {
                  respuesta = {"msg": "Error al actualizar usuario"};
                  res.status(500).send(respuesta);
                }
              });
          } else {
            respuesta = {"msg": "Usuario no encontrado."};
            res.status(404).send(respuesta);
          }
        }
      });
   } //FIN PUT modificarUsuario

/**
* @name borrarUsuario
* @param params.id (numérico)
* @version 1.0.0
* @type method
* @description Elimina un usuario existente que no tenga cuentas asociadas
* @author PCURBELO
*/
function borrarUsuario(req, res){
  var id = req.params.id;
  var queryStringID = 'q={"id":' + id + '}&';
  var httpClient = requestJSON.createClient(baseMLabURL);
  var respuesta = {};
  httpClient.get('user?' + queryStringID + apikeyMLab,
    function(error, respuestaMLab, body) {
      if (error) {
        respuesta = {"msg": "Error obteniendo usuario."};
        res.status(500).send(respuesta);
      } else {
        var queryStringIdUsu = 'q={"id_usuario":' + id + '}&';
        httpClient.get('cuenta?'+ queryStringIdUsu + apikeyMLab ,
          function(error1, respuestaMLab1 , body1) {
            if (error1) {
              respuesta = {"msg": "Error obteniendo cuentas de usuario."};
              res.status(500).send(respuesta);
            } else {
              if (body.length > 0 && body1.length == 0) {
                respuesta = body;
                httpClient.delete('user/' + body[0]._id.$oid + '?' + apikeyMLab,
                  function(error2, respuestaMLab2, body2) {
                  if (error2) {
                    respuesta = {"msg": "Error al borrar. "};
                    res.status(500).send(respuesta);
                  } else {
                    respuesta = {"msg": "Borrado correctamente. ", body2};
                    res.status(200).send(respuesta);
                  }
                });
              } else {
                if (body1.length != 0) {
                  respuesta = {"msg": "Usuario tiene cuentas asociadas, no puede borrarse.", body1};
                  res.status(401).send(respuesta);
                } else {
                  respuesta = {"msg": "Usuario no encontrado.", body};
                  res.status(404).send(respuesta);
                }
              }
            }
          });
        }
      });
} //FIN DELETE borrarUsuario

/**
* @name login
* @param body.email (string), body.password (string)
* @version 1.0.0
* @type method
* @description Loguea un usuario existente, tomando el email como y validando su contraseña
* @author PCURBELO
*/
function login(req, res){
  var respuesta = {};
  var email = req.body.email;
  var pass = req.body.password;
  var queryStringEmail = 'q={"email":"' + email + '"}&';
  var httpClient = requestJSON.createClient(baseMLabURL);
  httpClient.get('user?'+ queryStringEmail + apikeyMLab ,
  function(error, respuestaMLab , body) {
    if (error) {
      respuesta = {"msg": "Error obteniendo usuario."};
      res.status(500).send(respuesta);
    } else {
      if (body.length > 0) {
        if (body[0].password == pass) {
          var session = {"logged":true};
          var login = '{"$set":' + JSON.stringify(session) + '}';
          httpClient.put('user?q={"id": ' + body[0].id + '}&' + apikeyMLab, JSON.parse(login),
            function(errorP, respuestaMLabP, bodyP) {
                if (!errorP) {
                  respuesta = {
                    "msg": "Login Correcto",
                    "id": body[0].id,
                    "nombre": body[0].first_name,
                    "apellido": body[0].last_name,
                    "email": body[0].email
                  };
                  res.status(200).send(respuesta);
                } else {
                  respuesta = {"msg": "Error de Login."};
                  res.status(500).send(respuesta);
                }
            });
        } else {
          respuesta = {"msg":"Datos Incorrectos"};
          res.status(401).send(respuesta);
        }
      } else {
      respuesta = {"msg": "Datos Incorrectos"};
      res.status(401).send(respuesta);
    }
  }
  });
} //FIN GET/PUT login

/**
* @name logout
* @param body.email (string)
* @version 1.0.0
* @type method
* @description Desloguea un usuario logueado, recibiendo el email.
* @author PCURBELO
*/
function logout(req, res){
  var respuesta = {};
  var email = req.body.email;
  var queryStringEmail = 'q={"email":"' + email + '"}&';
  var httpClient = requestJSON.createClient(baseMLabURL);
  httpClient.get('user?'+ queryStringEmail + apikeyMLab ,
    function(error, respuestaMLab, body) {
      if (error) {
        respuesta = {"msg": "Error obteniendo usuario."};
        res.status(500).send(respuesta);
      } else {
        if (body.length > 0) {
          if(body[0].logged != undefined) {
            var session = {"logged":true};
            var logout = '{"$unset":' + JSON.stringify(session) + '}';
            httpClient.put('user?q={"id": ' + body[0].id + '}&' + apikeyMLab, JSON.parse(logout),
              function(errorP, respuestaMLabP, bodyP) {
                if (!errorP) {
                  respuesta = {"msg":"Logout Correcto"};
                  res.status(200).send(respuesta);
                } else {
                  respuesta = {"msg": "Error de Logout."};
                  res.status(500).send(respuesta);
                }
            });
          } else {
            respuesta = {"msg": "Error usuario no logueado"};
            res.status(401).send(respuesta);
          }
      } else {
        respuesta = {"msg": "Error en logout"};
        res.status(401).send(respuesta);
      }
    }
  });
}

//Metodos visibles
module.exports = {
  obtenerUsuarios,
  obtenerUsuario,
  crearUsuario,
  modificarUsuario,
  borrarUsuario,
  login,
  logout
}
