# Image docker base
FROM node:latest

# Directorio de trabajo de docker
WORKDIR /docker-api

# Copiar archivos del proyecto al directorio docker
ADD . /docker-api

# instalar dependencias del proyecto
# RUN npm install

# Puerto donde exponemos el contenedor
EXPOSE 3000

# Comando para lanzar la app
CMD ["npm" , "start"]
